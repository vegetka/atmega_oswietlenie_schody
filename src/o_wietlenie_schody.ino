#include <Arduino.h>

//(c) 2017 Arkadiusz 'vegetKa' Maciak
// version: 1.01.2017

int jasnosc = 0;
int ruch = 1;
int wartosc_jasnosci = 0;
int wartosc_ruchu = 0;
int lampki = 7;

void setup() {
  pinMode(lampki, OUTPUT);
  //Serial.begin(9600);
}

void loop() {
  wartosc_jasnosci = analogRead(jasnosc);
  wartosc_ruchu    = analogRead(ruch);
  //Serial.println(wartosc_jasnosci);
  //Serial.println(wartosc_ruchu);
  if (wartosc_jasnosci > 600 & wartosc_ruchu > 600) {
    digitalWrite(lampki, HIGH);
    delay(15000);
  } else {
    digitalWrite(lampki, LOW);
  }
}
